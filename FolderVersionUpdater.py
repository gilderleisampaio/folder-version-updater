from genericpath import exists
from imap_tools import MailBox,AND
from pyunpack import Archive
import shutil

import re
import os
import time
import datetime

###################################################################################  
# Mudar somente esses campos 
EMAIL_CONTA = 'gilderlei@se7esistemas.com.br'
EMAIL_SENHA = '#####'
CAMINHO_CRIAR_PASTA = 'F:\\Data7Versoes\\VersoesAutomaticas'
DIRETORIO_BIBLIOTECAS = 'F:\\Data7Versoes\\VersoesAutomaticas\\Bibliotecas'
CAMINHO_ARQUIVO_LOG = 'F:\\Data7Versoes\\VersoesAutomaticas\\Log.txt'
###################################################################################

SUBJECT = 'Compilação do Data7 realizada com sucesso!' 
TEMPO_EM_SEGUNDOS_PARA_LOOP = 10
ultima_contagem = time.time()
rc = None

def escreverLog(texto_do_log):
    with open(CAMINHO_ARQUIVO_LOG, 'a') as arqlog:
        arqlog.write(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")+' -> '+texto_do_log+'\n')
        print(texto_do_log)

while 1 == 1: 
    if (time.time() - ultima_contagem) >= TEMPO_EM_SEGUNDOS_PARA_LOOP:

        with open(CAMINHO_ARQUIVO_LOG, 'a') as arqlog:
            pass

        with MailBox('imap.se7esistemas.com.br').login(EMAIL_CONTA, EMAIL_SENHA) as mailbox:
            mailbox.folder.set('Se7e_Nao_Responda')
            messages = mailbox.fetch(criteria=AND(seen=False, from_="Se7e Builder"), mark_seen=True, bulk=True)
        
            caminho_versao_localizado = ''
            caminho_biblioteca_localizado = ''
            nome_da_pasta_criar = ''
            nome_da_pasta_agrupadora = ''    
            pasta_raiz_bibliotecas = ''
            diretorio_destino_Versao = ''
            caminho_absoluto_arquivo_versao = ''
            diretorio_destino_DLLs = ''
            local_descompactar_arquivo_bibliotecas = ''

            for msg in messages:
                if SUBJECT in msg.html:
                    textoEncontrado = str(re.compile(r"(>\\.+?Setup Data7.+?exe)").findall(msg.html)).replace('>','')        
                    caminho_versao_localizado = textoEncontrado.replace('arquivos','192.168.5.7').replace('Setup ','').replace('exe','7z').strip('[]').strip("'").replace('\\\\','\\')
                    nome_da_pasta_criar = re.compile(r'((.+?)\\\\)+').match(str(textoEncontrado)).group(2)
                    pasta_raiz_bibliotecas = str(re.compile(r'\\.+?TAG.+?\\').findall(caminho_versao_localizado)).strip('[]').strip("'").replace('\\\\','\\')

                if nome_da_pasta_criar != '':

                    if len(nome_da_pasta_criar) > 6:
                        nome_da_pasta_agrupadora = re.compile(r"(.([0-9]{3}).)").search(nome_da_pasta_criar).group(2)
                        diretorio_destino_Versao = CAMINHO_CRIAR_PASTA+'\\'+nome_da_pasta_agrupadora+'\\'+nome_da_pasta_criar
                        caminho_absoluto_arquivo_versao =  CAMINHO_CRIAR_PASTA+'\\'+nome_da_pasta_agrupadora+'\\'+nome_da_pasta_criar+'\\'+nome_da_pasta_criar+'.7z'
                    else:
                        diretorio_destino_Versao = CAMINHO_CRIAR_PASTA+'\\'+nome_da_pasta_criar
                        caminho_absoluto_arquivo_versao =  CAMINHO_CRIAR_PASTA+'\\'+nome_da_pasta_criar+'\\'+nome_da_pasta_criar+'.7z'

                    if os.path.isdir(diretorio_destino_Versao):
                        try:
                            escreverLog("Iniciando Processo...")
                            escreverLog("Removendo pasta existente "+diretorio_destino_Versao)
                            shutil.rmtree(diretorio_destino_Versao)
                        except OSError as excep:
                            escreverLog("Houve erro ao tentar deletar a pasta de destino da versao.\nVerifique as permissões ou se a pasta está em uso. Erro: " + str(excep.errno) + " " + excep.strerror)
                            continue
                        
                    try:
                        escreverLog("criando novo diretorio: "+ diretorio_destino_Versao)
                        os.makedirs(diretorio_destino_Versao,exist_ok=True)
                    except (FileExistsError,OSError) as exce:
                        escreverLog("Erro ao criar a pasta de destino da versao "+exce.strerror)
                        continue

                    try:
                        escreverLog("copiando versao")
                        #copia versao
                        shutil.copyfile(caminho_versao_localizado, caminho_absoluto_arquivo_versao)
                    except Exception as exce:
                        escreverLog("Erro ao copiar os arquivos da versao. Erro "+ str(exce))
                        continue
    
                    try:
                        escreverLog("descompactando versão")
                        #descompacta versao
                        Archive(caminho_absoluto_arquivo_versao).extractall(diretorio_destino_Versao)
                    except Exception as exce:
                        escreverLog("Erro ao descompactar os arquivos da versao. Erro: "+str(exce))
                        continue


                    for arq in os.listdir(pasta_raiz_bibliotecas):
                        if "Bibliotecas_Data7" in arq:
                            caminho_biblioteca_localizado = pasta_raiz_bibliotecas+arq
                            diretorio_destino_DLLs = DIRETORIO_BIBLIOTECAS+'\\'+arq

                            if os.path.isdir(diretorio_destino_Versao+'\\'+'Data7\\bin'):
                                local_descompactar_arquivo_bibliotecas = diretorio_destino_Versao+'\\'+'Data7\\bin'
                            else:
                                local_descompactar_arquivo_bibliotecas = diretorio_destino_Versao+'\\'+'Data7'

                

                    if not os.path.isfile(diretorio_destino_DLLs):
                        try:
                            escreverLog("copiando bibliotecas")
                            #copia bibliotecas
                            shutil.copyfile(caminho_biblioteca_localizado, diretorio_destino_DLLs)
                        except Exception as exce:
                            escreverLog("Erro ao copiar os arquivos de bibliotecas. Erro: "+str(exce))
                            continue
    
                    try:
                        escreverLog("descompactando bibliotecas")
                        #descompacta bibliotecas
                        Archive(diretorio_destino_DLLs).extractall(local_descompactar_arquivo_bibliotecas)
                    except Exception as exce:
                        escreverLog("Erro ao descompactar os arquivos de bibliotecas. Erro: "+str(exce))
                        continue

                escreverLog("Processo Finalizado")
        ultima_contagem = time.time()

    time.sleep(1)   